# **Puppet Basics**


## **STEP 1: Install Puppet On Host**

### **Ubuntu Host**

```bash
./puppet_install.sh
```

## **STEP 2: Install Virtualbox**

### **Ubuntu**

For Virtualbox, some things need to be met:

* Virtual features in BIOS enabled
* Trusted Computing disabled (blocks hypervisor)
* Cannot use KVM or Xen in conjunction with Virtualbox

With these met, you can install Virtualbox:

```bash
# Install Virtualbox for Vagrant
VBOX_DEB_KEY="https://www.virtualbox.org/download/oracle_vbox_2016.asc"
VBOX_DEB_SRC='deb http://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib'
wget -q ${VBOX_DEB_KEY}-O- | sudo apt-key add -
sudo echo ${VBOX_DEB_SRC} > /etc/apt/sources.list.d/virtualbox.list
sudo apt update
sudo apt install -y virtualbox-5.2
```

## **STEP 3: Install Vagrant**

```bash
VERSION='2.0.4'
PACKAGE="vagrant_${VERSION}_x86_64.deb"
curl -O https://releases.hashicorp.com/vagrant/${VERSION}/${PACKAGE}
sudo dpkg -i ${PACKAGE}
```

## **STEP 4: Download, Start, Log into Virtual Guest**

```bash
vagrant up
vagrant ssh
```

## **STEP 5: Install Puppet Agent on Guest**

```bash
/vagrant/puppet_agent.sh
```

## **STEP 6: Configure a Resource**

### **Hello World***
Create `hello.pp`:

```puppet
file { '/tmp/motd':
  content => 'hello world'
}
```

```bash
$ puppet apply hello.pp
$ more /tmp/motd
```

### **Goodbye World**

Create `goodbye.pp`:

```puppet
file { '/tmp/motd':
  ensure => absent
}
```

```bash
$ puppet apply goodbye.pp
$ more /tmp/motd
/tmp/motd: No such file or directory
```

## **STEP 7: Configure a Package and Service**

### **Package**

Create `webserver.pp`:

```puppet
package { 'apache2':
  ensure => present,
}
```

```bash
sudo puppet apply webserver.pp
```

### **Service**

Update `webserver.pp`:
```puppet
package { 'apache2':
  ensure => present,
}

service { 'apache2':
  ensure => running,
  enable => true,
}
```

```bash
sudo puppet apply webserver.pp
```

### **Content**

Update `webserver.pp`:
```puppet
package { 'apache2':
  ensure => present,
}

service { 'apache2':
  ensure => running,
  enable => true,
}

file { '/var/www/html/index.html':
  content => '<html>
  <body>
    <h1>hello world</h1>
  </body>
</html>',
}
```

```bash
$ sudo puppet apply webserver.pp
$ curl localhost
```

## **STEP 7: Make Manifest more Manageable**


```bash
$ mkdir site && cd site
$ puppet module generate learn/learn_puppet_apache2 --skip-interview
```

```bash
$ tree site
site
└── learn_puppet_apache2
    ├── examples
    │   └── init.pp
    ├── Gemfile
    ├── manifests
    │   └── init.pp
    ├── metadata.json
    ├── Rakefile
    ├── README.md
    └── spec
        ├── classes
        │   └── init_spec.rb
        └── spec_helper.rb

5 directories, 8 files
```

### **Template**

Create a template:

```bash
mkdir ~/site/learn_puppet_apache2/templates
cat <<-EOF > ~/site/learn_puppet_apache2/templates/index.html.erb
<html>
  <body>
    <h1>hello world</h1>
  </body>
</html>
EOF
```

Edit  `~/site/learn_puppet_apache2/manifests/init.pp`:

```
class learn_puppet_apache2 {
  package { 'apache2':
    ensure => present,
  }

  service { 'apache2':
    ensure => running,
    enable => true,
  }

  file { '/var/www/html/index.html':
    content => template('learn_puppet_apache2/index.html.erb'),
  }

}
```

```bash
sudo puppet module install puppetlabs-stdlib --modulepath=${HOME}/site # workaround for PUP-3121
sudo puppet apply --modulepath=${HOME}/site -e 'include ::learn_puppet_apache2'
```



## **RESOURCES**

* http://fullstack-puppet-docs.readthedocs.io/en/latest/puppet_modules.html
* http://fullstack-puppet-docs.readthedocs.io/en/latest/puppet_manifests.html
* https://www.digitalocean.com/community/tutorials/getting-started-with-puppet-code-manifests-and-modules
* https://tickets.puppetlabs.com/browse/PUP-3121
