package { 'apache2':
  ensure => present,
}

service { 'apache2':
  ensure => running,
  enable => true,
}

file { '/var/www/html/index.html':
  content => '<html>
  <body>
    <h1>hello world</h1>
  </body>
</html>',
}
